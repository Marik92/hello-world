FROM alpine:3.7

MAINTAINER Okassov Marat

COPY target/app /usr/local/bin/app

ENTRYPOINT ["/usr/local/bin/app"]
